package aplicacion;

import java.io.*;
import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;

public class EjemploCachedRowSet {

	// fichero donde se guarda el CachedRowSet serializado
	private final static String ARCHIVO_CRS = "datoscrs.dat";

	public static void main(String[] args) {
		// Creamos un objeto CacheRowSetImpl serializado en un fichero
		String jdbcUrl = "jdbc:mariadb://192.168.56.102:3306/empresa_ad";
		String usr = "batoi";
		String pw = "1234";

		// Instanciamos un objeto CachedRowSetImpl y parametrizamos la conexion
		try (CachedRowSet crs = RowSetProvider.newFactory().createCachedRowSet()) {

			// Sería posible transformar un Resultset en un cachedrowset.
			// ResultSet rs = ConexionBD.getConexion().createStatement().executeQuery("SELECT * FROM articulos");
			// crs.populate(rs);

			crs.setUrl(jdbcUrl);
			crs.setUsername(usr);
			crs.setPassword(pw);

			crs.setCommand("select * from articulos order by id");
			// crs.setKeyColumns(new int[] { 1 }); // podemos indicar que columna/s que forman parte de la clave (no obligatorio)
			crs.execute();
			mostrar(crs);

			// insercion
			crs.moveToInsertRow();
			crs.updateNull(1);
			crs.updateString(2, "arti1");
			crs.updateFloat(3, 100f);
			crs.updateString(4, "art1");
			crs.updateInt(5, 1);
			crs.updateInt(6, 10);
			crs.insertRow();
			crs.moveToCurrentRow();
			crs.acceptChanges();
			
			// modificación
			crs.last();
			crs.updateFloat("precio", crs.getFloat(("precio")) + 200);
			crs.updateRow();
			crs.acceptChanges();
			
			mostrar(crs);     
            
            // Lo serializamos en un fichero
			System.out.println("Serializando...");
			serializaCachedRowSet(crs);

			// Leyendo de archivo serializado...
			System.out.println("Deserializando...");
			CachedRowSet crs2 = leeCachedRowSet();
			mostrar(crs2);	
			
			// Ejecución de un comando concreto dml: este sí que se persiste en la fuente
			System.out.println("Ejecución instrucción dml");
			crs.setCommand("insert into empresa_ad.articulos(nombre, precio, codigo, grupo) values ('uno', 1, 'cod_uno', 1)");
			crs.execute();			

		} catch (SQLException se) {
			System.err.println(se.getMessage());
		}
	}

	private static void serializaCachedRowSet(CachedRowSet crs) {

		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(ARCHIVO_CRS, false))) {
			oos.writeObject(crs);
			System.out.println("¡¡Serializado con éxito!!");
			System.out.println("--------------------------------------");
		} catch (IOException ex) {
			System.err.println(ex.getMessage());
		}
	}

	public static CachedRowSet leeCachedRowSet() {
		CachedRowSet crs = null;
		// Leemos el objeto CachedRowSetImpl serializado desde el archivo
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(ARCHIVO_CRS))) {
			crs = (CachedRowSet) ois.readObject();
		} catch (FileNotFoundException ex) {
			System.out.println("Fichero no existe: " + ex.getMessage());
		} catch (IOException | ClassNotFoundException ex) {
			System.err.println("Error E/S: " + ex.getMessage());
		}
		return crs;
	}

	private static void mostrar(CachedRowSet crs) throws SQLException {
		if (crs != null) {
			crs.beforeFirst();
			while (crs.next()) {
				System.out.print("ID: " + crs.getInt("id"));
				System.out.print(", Nombre: " + crs.getString("nombre"));
				System.out.print(", Precio: " + crs.getFloat("precio"));
				System.out.print(", Codigo: " + crs.getString("codigo"));
				System.out.print(", Grupo: " + crs.getInt("grupo"));
				System.out.println();
			}
		} else {
			System.out.println("CachedRowSet es null");
		}
		System.out.println("--------------------------------------");
	}
}
