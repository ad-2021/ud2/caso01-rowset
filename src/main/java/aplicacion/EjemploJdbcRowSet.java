package aplicacion;

import java.sql.*;

import javax.sql.RowSetEvent;
import javax.sql.RowSetListener;
import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;

public class EjemploJdbcRowSet {

	public static void main(String[] args) throws InterruptedException {
		String jdbcUrl = "jdbc:mariadb://192.168.56.102:3306/empresa_ad";
//		String jdbcUrl = "jdbc:postgresql://192.168.56.102:5432/batoi?currentSchema=empresa_ad";
		String usr = "batoi";
		String pw = "1234";

		try (JdbcRowSet jrs = RowSetProvider.newFactory().createJdbcRowSet()) {
			jrs.setUrl(jdbcUrl);
			jrs.setUsername(usr);
			jrs.setPassword(pw);

			// Añadir listener
//			anyadirListener(jrs);

			// Consultas
			jrs.setCommand("select * from empresa_ad.clientes");
			jrs.execute();
			mostrar(jrs);

//			// Inserción
			System.out.println("Insertando...");
			jrs.moveToInsertRow();
			jrs.updateString(2, "Sergio");
			jrs.updateString(3, "C/ laquesea, 2");
			jrs.insertRow();
			jrs.moveToCurrentRow();
			System.out.println("Fila insertada!");
			mostrar(jrs);

//			// Modificación
			System.out.println("Modificando...");
			jrs.last();
			jrs.updateString(2, "Sergio modificado");
			jrs.updateRow();
			System.out.println("Fila actualizada!");
			mostrar(jrs);

//			// Borrado
			System.out.println("Borrando...");
			jrs.last();
			jrs.deleteRow();
			System.out.println("Fila borrada!");
			mostrar(jrs);

			System.out.println("Consulta parametrizada");
			jrs.setCommand("select * from empresa_ad.clientes where nombre like ?");
			jrs.setString(1, "John%");
			jrs.execute();
			mostrar(jrs);

			// Ejecución de otros comandos dml
			System.out.println("Insertando con dml...");
			jrs.setCommand("INSERT INTO empresa_ad.clientes(nombre, direccion) VALUES ('Carlos','Calle xxxx')");			
			jrs.execute();
			System.out.println("Fila insertada!");

			// Llamadas a procedimientos. ¿parámetro de salida?
			System.out.println("Llamada a procedimiento almacenados");
//			jrs.setCommand("call ejemplo(?,?)");
//			jrs.setInt(1, 10);
//			jrs.setInt(2, 3);
//			jrs.execute();

		} catch (SQLException se) {
			System.err.println("Error: " + se.getMessage());
		}
	}

	private static void anyadirListener(JdbcRowSet jrs) {

		jrs.addRowSetListener(new RowSetListener() {
			@Override
			public void rowSetChanged(RowSetEvent event) {
				System.out.println("\tRowset manipulado!");
			}

			@Override
			public void rowChanged(RowSetEvent event) {
				JdbcRowSet j = (JdbcRowSet) event.getSource();
				try {
					System.out.println("\tFila cambiada! -> " + j.getRow());
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}

			@Override
			public void cursorMoved(RowSetEvent event) {
				JdbcRowSet j = (JdbcRowSet) event.getSource();
				try {
					System.out.println("\tCursor se ha movido! -> " + j.getRow());
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		});

	}

	private static void mostrar(JdbcRowSet jrs) throws SQLException {
		jrs.beforeFirst();
		while (jrs.next()) {
			System.out.print("ID: " + jrs.getInt(1));
			System.out.print(", Nombre: " + jrs.getString(2));
			System.out.println();
		}
		System.out.println("------------------------------------------------");
	}
}
